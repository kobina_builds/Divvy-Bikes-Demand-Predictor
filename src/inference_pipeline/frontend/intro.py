import streamlit as st 


st.header(":red[Welcome] :orange[to the] :green[Hourly] :blue[Divvy Trip] :violet[Predictor]")

st.markdown(
    """
    This service provides hourly :violet[predictions] of the number of :green[arrivals] and :orange[departures] at various 
    :blue[Divvy stations] around the :blue[city of Chicago]. It is the culmination of an :red[end-to-end] machine learning system trained on
    publicly available :blue[Divvy] trip data.

    Take a look at the :violet["Predictions"] page, where we provide the main results of the model.
    """
)


    